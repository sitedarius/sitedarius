<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('home');
Route::get('/afgeronde-projecten/', 'ProjectsController@index')->name('projects');
Route::get('/over-ons/', 'PagesController@about')->name('about');

Auth::routes();

Route::post('/projecten/toevoegen', 'ProjectsController@store')->name('store.project');
Route::delete('/projecten/{id}/verwijderen', 'ProjectsController@destroy')->name('delete.project');
Route::patch('/projecten/{id}/bewerken', 'ProjectsController@update')->name('update.project');

Route::post('/bericht/toevoegen', 'PostController@store')->name('store.post');

Route::post('/medewerker/toevoegen', 'WorkerController@store')->name('store.worker');
Route::delete('/medewerker/{id}/verwijderen', 'WorkerController@destroy')->name('delete.worker');
Route::patch('/medewerker/{id}/bewerken', 'WorkerController@update')->name('update.worker');
