<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="{{ route('home') }}"><i class="fas fa-pen-nib"></i></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" type="button" data-toggle="modal" data-target="#bericht"><i class="fas fa-plus"></i>
                    Bericht</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" type="button" data-toggle="modal" data-target="#project"><i class="fas fa-plus"></i>
                    Project</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" type="button" data-toggle="modal" data-target="#worker"><i class="fas fa-plus"></i>
                    Collega</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#"><i class="fas fa-plus"></i> Vacature</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#"><i class="fas fa-plus"></i> Pagina</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link mr-3" href="#"><i class="fas fa-external-link-alt"></i> Portfolio</a>
            </li>
            <li class="nav-item">
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                   class="nav-link text-danger ml-0">
                    <i class="fas fa-sign-out-alt"></i>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </div>
</nav>

<div class="modal fade" id="bericht" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Voeg een <strong>bericht</strong> toe</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('store.post') }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="title" class="font-weight-bold mb-0">Titel:*</label>
                        <input class="form-control mb-3" type="text" id="title" name="title">
                    </div>
                    <div class="form-group">
                        <label for="body" class="font-weight-bold mb-0">Inhoud:*</label>
                        <textarea class="form-control mb-3" id="body" rows="3" name="body"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="file" class="font-weight-bold mb-0">Bestand:*</label>
                        <input type="file" class="form-control-file mb-3" id="file" name="image">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Opslaan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="project" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Voeg een <strong>project</strong> toe</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('store.project') }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="title" class="font-weight-bold mb-0">Titel:</label>
                        <input class="form-control mb-3" type="text" id="title" name="title">
                    </div>
                    <div class="form-group">
                        <label for="body" class="font-weight-bold mb-0">Inhoud:</label>
                        <textarea class="form-control mb-3" id="body" rows="3" name="body"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="title" class="font-weight-bold mb-0">Website:*</label>
                        <input class="form-control mb-3" type="text" id="title" name="website">
                    </div>
                    <div class="form-group">
                        <label for="file" class="font-weight-bold mb-0">Logo:*</label>
                        <input type="file" class="form-control-file mb-3" id="file" name="logo">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Opslaan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="worker" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Voeg een <strong>collega</strong> toe</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form  method="POST" action="{{ route('store.worker') }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="title" class="font-weight-bold mb-0">Naam:*</label>
                        <input class="form-control mb-3" type="text" id="title" name="name">
                    </div>
                    <div class="form-group">
                        <label for="title" class="font-weight-bold mb-0">Functie:*</label>
                        <input class="form-control mb-3" type="text" id="title" name="role">
                    </div>
                    <div class="form-group">
                        <label for="file" class="font-weight-bold mb-0">Foto:*</label>
                        <input type="file" class="form-control-file mb-3" id="file" name="image">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Opslaan</button>
                </div>
            </form>

        </div>
    </div>
</div>
