{{--<section class="header">--}}
{{--    <div class="col-md-4 py-4">--}}
{{--        <a href="{{ route('home') }}"><img src="{{ asset('storage/img/') }}/logo.svg" class="brand mt-1"></a>--}}
{{--    </div>--}}
{{--</section>--}}

{{--<nav class="navbar navbar-expand-lg navbar-dark bg-main">--}}
{{--    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">--}}
{{--        <span class="navbar-toggler-icon"></span>--}}
{{--    </button>--}}

{{--    <div class="collapse navbar-collapse" id="navbarSupportedContent">--}}
{{--        <ul class="navbar-nav mx-auto">--}}
{{--            <li class="nav-item mx-3">--}}
{{--                <a class="nav-link" href="{{ route('home') }}">Home</a>--}}
{{--            </li>--}}
{{--            <li class="nav-item mx-3">--}}
{{--                <a class="nav-link" href="{{ route('about') }}">Over Ons</a>--}}
{{--            </li>--}}
{{--            <li class="nav-item mx-3">--}}
{{--                <a class="nav-link" href="{{ route('projects') }}">Projecten</a>--}}
{{--            </li>--}}
{{--            <li class="nav-item mx-3">--}}
{{--                <a class="nav-link" href="#">Contact</a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </div>--}}
{{--</nav>--}}

<nav class="navbar navbar-expand-lg navbar-light bg-white shadow main-nav py-4 sticky-top">
    <div class="container">
    <a class="navbar-brand" href="{{ route('home') }}"><img src="/storage/img/logo.svg" class="w-100"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto font-">
            <li class="nav-item active">
                <a class="nav-link font-weight-bold" href="{{ route('home') }}">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link font-weight-bold" href="{{ route('about') }}">Over ons</a>
            </li>
            <li class="nav-item">
                <a class="nav-link font-weight-bold" href="{{ route('projects') }}">Projecten</a>
            </li>
            <li class="nav-item">
                <a class="nav-link font-weight-bold" href="#">Service</a>
            </li>
            <li class="nav-item">
                <a class="nav-link font-weight-bold" href="#">Contact</a>
            </li>
            <button class="btn btn-main ml-3">Offerte Aanvragen</button>
            <li class="nav-item">
                <a class="nav-link font-weight-bold ml-3 text-dark" href="#"><i class="fas fa-search"></i></a>
            </li>
        </ul>
    </div>
    </div>
</nav>

<img src="/storage/img/index1.jpg" class="img w-100" style="">

