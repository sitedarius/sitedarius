@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @foreach($projects as $project)
                @if($project->body === null && $project->is_active === 1)
                    <div class="col-md-3 col-4 mt-3 p-3">
                        @if(Auth::user())
                            @if(Auth::user()->is_admin === 1)
                                <form method="POST" action="{{ route('delete.project', ['id' => $project->id]) }}">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-link"><i class="fas fa-trash-alt"></i></button>
                                </form>
                                <a href="" data-toggle="modal" data-target="#staticBackdrop"><i class="fas fa-pencil-alt"></i></a>
                            @endif
                        @endif
                        <a href="http://{{ $project->website }}" target="_blank">
                            <img class="w-100" alt="{{ $project->logo }}" src="/storage/img/uploads/{{ $project->logo }}">
                        </a>
                    </div>
                @endif
            @endforeach

            <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Bewerk <strong>project</strong>:</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="POST" action="{{ route('update.project', ['id' => $project->id]) }}" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="title" class="font-weight-bold mb-0">Titel:</label>
                                    <input class="form-control mb-3" type="text" id="title" name="title" value="{{ $project->title }}">
                                </div>
                                <div class="form-group">
                                    <label for="body" class="font-weight-bold mb-0">Inhoud:</label>
                                    <textarea class="form-control mb-3" id="body" rows="3" name="body" value="{{ $project->body }}"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="font-weight-bold mb-0">Website:*</label>
                                    <input class="form-control mb-3" type="text" id="title" name="website" value="{{ $project->website }}">
                                </div>
                                <div class="form-group">
                                    <label for="file" class="font-weight-bold mb-0">Logo:*</label>
                                    <input type="file" class="form-control-file mb-3" id="file" name="logo" value="{{ $project->logo }}">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Opslaan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
