@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            @foreach($posts->take(3) as $post)
                <div class="col-md-4">
                    <div class="single-blog">
                        <div class="blog-img">
                            <img src="/storage/img/uploads/{{ $post->image }}" alt="">
                            <div class="post-category">
                                <a href="#">Creative</a>
                            </div>
                        </div>
                        <div class="blog-content">
                            <div class="blog-title">
                                <h4><a href="#">{{ $post->title }}</a></h4>
                                <div class="meta">
                                    <ul>
                                        <li>{{ Carbon\Carbon::parse($post->created_at)->format('l jS F Y h:i:s') }}</li>
                                    </ul>
                                </div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque.</p>
                            <a href="#" class="box_btn">read more</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
