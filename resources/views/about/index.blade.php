@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <section class="team-area pt-100 pb-100 w-100" id="team">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <h4>mijn team</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @foreach($workers as $worker)
                        @if($worker->is_active === 1)
                            <div class="col-md-4">
                                <div class="single-team">
                                    <img src="storage/img/uploads/{{ $worker->image }}"
                                         alt="">
                                    <div class="team-hover">
                                        <h4>{{ $worker->name }} <span>{{ $worker->role }}</span></h4>
                                        <a href=""><i class="fa fa-facebook"></i></a>
                                        <a href=""><i class="fa fa-twitter"></i></a>
                                        <a href=""><i class="fa fa-youtube"></i></a>
                                        <a href=""><i class="fa fa-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </section>


            @foreach($workers as $worker)
                @if($worker->is_active === 1)
                    <div class="col-md-3 col-4 mt-3 p-3">
                        @if(Auth::user())
                            @if(Auth::user()->is_admin === 1)
                                <form method="POST" action="{{ route('delete.worker', ['id' => $worker->id]) }}">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-link"><i class="fas fa-trash-alt"></i></button>
                                </form>
                                <a href="" data-toggle="modal" data-target="#updateworker"><i
                                        class="fas fa-pencil-alt"></i></a>
                            @endif
                        @endif
                        <div class="card">
                            <img src="/storage/img/uploads/{{ $worker->image }}" class="w-100 rounded-top"
                                 alt="{{ $worker->image }}">
                            <div class="card-body text-center">
                                <h5 class="font-weight-bold">{{ $worker->name }}</h5>
                                <hr class="my-1">
                                <p class="font-italic mb-1">{{ $worker->role }}</p>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach

            <div class="modal fade" id="updateworker" data-backdrop="static" tabindex="-1" role="dialog"
                 aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Bewerk een <strong>medewerker</strong>:
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="POST" action="{{ route('update.worker', ['id' => $worker->id]) }}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="title" class="font-weight-bold mb-0">Naam:*</label>
                                    <input class="form-control mb-3" type="text" id="title" name="name"
                                           value="{{ $worker->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="title" class="font-weight-bold mb-0">Functie:*</label>
                                    <input class="form-control mb-3" type="text" id="title" name="role"
                                           value="{{ $worker->role }}">
                                </div>
                                <div class="form-group">
                                    <label for="file" class="font-weight-bold mb-0">Foto:*</label>
                                    <input type="file" class="form-control-file mb-3" id="file" name="image"
                                           value="{{ $worker->image }}">
                                </div>
                                <label for="img" class="font-weight-bold mb-0">Huidige Foto:</label><br>
                                <img src="/storage/img/uploads/{{ $worker->image }}" class="w-25">
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Opslaan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
