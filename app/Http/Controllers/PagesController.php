<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Project;
use App\Worker;

class PagesController extends Controller
{
    public function index()
    {
        $projects = Project::all();
        $posts = Post::all();
        $workers = Worker::all();

        return view('index', compact('posts', 'projects', 'workers'));
    }

    public function about()
    {
        $workers = Worker::all();

        return view('about.index', compact('workers'));
    }
}
