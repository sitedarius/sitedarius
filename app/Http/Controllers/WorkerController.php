<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Worker;

class WorkerController extends Controller
{
    public function store(Request $request)
    {
//        request()->validate([
//            'name' => 'required',
//            'description_short' => 'required',
//            'description_long' => 'required',
//        ]);

        $worker = new Worker();
        $worker->name = request('name');
        $worker->role = request('role');
//        $post->user_id = Auth::user()->id;

        if ($request->image) {
            $image = $request->file('image');
            $logoName = time() . '.' . request()->image->getClientOriginalName();
            $image->move(public_path('storage/img/uploads'), $logoName);
            $worker->image = $logoName;
        }

        $worker->save();

        return back();
    }

    public function update($id, Request $request)
    {
//        request()->validate([
//            'name' => 'required',
//            'description_short' => 'required',
//            'description_long' => 'required'
//        ]);

        $worker = Worker::findOrFail($id);

        $worker->name = request('name');
        $worker->role = request('role');

        if ($request->image) {
            $image = $request->file('image');
            $logoName = time() . '.' . request()->logo->getClientOriginalName();
            $image->move(public_path('storage/img/uploads'), $logoName);
            $worker->image = $logoName;
        }

        $worker->save();

        return back();
    }

    public function destroy($id)
    {
        $worker = Worker::findOrFail($id);
        $worker->is_active = 0;

        $worker->save();

        return back();
    }
}
