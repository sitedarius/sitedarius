<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    public function index()
    {
        $projects = Project::all();

        return view('projects.index', compact('projects'));
    }

    public function create()
    {
        return view('projects.create');
    }

    public function store(Request $request)
    {
//        request()->validate([
//            'name' => 'required',
//            'description_short' => 'required',
//            'description_long' => 'required',
//        ]);

        $project = new Project();
        $project->title = request('title');
        $project->body = request('body');
        $project->website = request('website');
//        $post->user_id = Auth::user()->id;

        if ($request->logo) {
            $image = $request->file('logo');
            $logoName = time() . '.' . request()->logo->getClientOriginalName();
            $image->move(public_path('storage/img/uploads'), $logoName);
            $project->logo = $logoName;
        }

        $project->save();

        return back();
    }

    public function update($id, Request $request)
    {
//        request()->validate([
//            'name' => 'required',
//            'description_short' => 'required',
//            'description_long' => 'required'
//        ]);

        $project = Project::findOrFail($id);

        $project->title = request('title');
        $project->body = request('body');
        $project->website = request('website');

        if ($request->logo) {
            $image = $request->file('logo');
            $logoName = time() . '.' . request()->logo->getClientOriginalName();
            $image->move(public_path('storage/img/uploads'), $logoName);
            $project->logo = $logoName;
        }

        $project->save();

        return back();
    }

    public function destroy($id)
    {
        $project = Project::findOrFail($id);
        $project->is_active = 0;

        $project->save();

        return back();
    }
}
