<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    public function index()
    {
        $post = Post::class()->all();

        return view('post.index', compact('post'));
    }

    public function store(Request $request)
    {
//        request()->validate([
//            'name' => 'required',
//            'description_short' => 'required',
//            'description_long' => 'required',
//        ]);

        $post = new Post();
        $post->title = request('title');
        $post->body = request('body');
//        $post->user_id = Auth::user()->id;

        if ($request->image) {
            $image = $request->file('image');
            $logoName = time() . '.' . request()->image->getClientOriginalName();
            $image->move(public_path('storage/img/uploads'), $logoName);
            $post->image = $logoName;
        }

        $post->save();

        return back();
    }
}
